package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    Map<String, Object> list(Integer page, Integer rows, String  customerName);

    ServiceVO save(Customer customer,Integer customerId);

    ServiceVO delete(String ids);
}
