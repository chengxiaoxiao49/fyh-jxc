package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr) {
        User user = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());
        Integer userId = user.getUserId();
        Gson gson = new Gson();
        List<OverflowListGoods> overflowListGoodsList = gson.fromJson(overflowListGoodsStr,new TypeToken<List<OverflowListGoods>>(){}.getType());
        overflowList.setUserId(userId);
        // 保存商品报溢单
        overflowListGoodsDao.saveOverflowList(overflowList);

        for (OverflowListGoods overflowListGoods : overflowListGoodsList) {
            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            overflowListGoodsDao.saveOverflowListGoods(overflowListGoods);

            // 获取Goods
            Goods goods = goodsDao.findByGoodsId(overflowListGoods.getGoodsId());
            goods.setInventoryQuantity(goods.getInventoryQuantity() + overflowListGoods.getGoodsNum());
            goods.setState(2);
            goodsDao.updateGoods(goods);
        }
        return new ServiceVO(100,"请求成功",null);
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> overflowListLists = overflowListGoodsDao.findByTimeList(sTime,eTime);
        User user = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());
        List<OverflowList> overflowListList = overflowListLists.stream().map(overflowList -> {
            overflowList.setTrueName(user.getTrueName());
            return overflowList;
        }).collect(Collectors.toList());
        map.put("rows",overflowListList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.findByOverflowListId(overflowListId);
        map.put("rows",overflowListGoodsList);
        return map;
    }


}
