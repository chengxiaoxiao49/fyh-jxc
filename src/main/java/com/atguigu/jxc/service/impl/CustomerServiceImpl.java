package com.atguigu.jxc.service.impl;


import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String  customerName) {
        Map<String,Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int pageNo = (page-1) * rows;
        List<Customer> customerDaoList = customerDao.findCustomerDaoList(pageNo,rows,customerName);
        map.put("total",customerDaoList.size());
        map.put("rows",customerDaoList);
        return map;
    }

    @Override
    public ServiceVO save(Customer customer,Integer customerId) {
        if (customerId == null){ // 执行添加操作
            customerDao.insert(customer);
        } else { // 执行修改操作
            customerDao.update(customer,customerId);
        }
        return new ServiceVO(100,"请求成功",null);
    }

    @Override
    public ServiceVO delete(String ids) {
        customerDao.delete(ids);
        return new ServiceVO(100,"请求成功",null);
    }
}
