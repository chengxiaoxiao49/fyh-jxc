package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;


    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {
        Map<String,Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int pageNo = (page-1)*rows;
        List<Supplier> supplierList = supplierDao.getSupplierPageList(pageNo,rows,supplierName);

        map.put("total",supplierList.size());
        map.put("rows",supplierList);

        return map;
    }


    @Override
    public ServiceVO save(Supplier supplier, Integer supplierId) {
        if (supplierId == null){ // 执行添加操作
            supplierDao.insert(supplier);
        } else { // 执行修改操作
            supplierDao.update(supplier,supplierId);
        }
        return new ServiceVO(100,"请求成功",null);
    }

    @Override
    public ServiceVO delete(String ids) {
        supplierDao.delete(ids);
        return new ServiceVO(100,"请求成功",null);
    }


}
