package com.atguigu.jxc.service.impl;



import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.entity.PurchaseList;
import com.atguigu.jxc.entity.SaleListGoods;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private LogService logService;

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String,Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int pageNo = (page-1) * rows;
        List<Goods> goodsList = goodsDao.findNameAndTypeId(pageNo,rows,codeOrName,goodsTypeId);
        int total = goodsDao.findTotal(codeOrName,goodsTypeId);
        map.put("total",total);
        for (Goods goods : goodsList){
            Integer goodsId = goods.getGoodsId();
            Integer saleTotal = goodsDao.getSaleTotal(goodsId);
            goods.setSaleTotal(saleTotal);
        }
        map.put("rows",goodsList);

        logService.save(new Log(Log.SELECT_ACTION,"分页查询商品信息"));
        return map;
    }

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String, Object> map = listInventory(page, rows, goodsName, goodsTypeId);
        return map;
    }

    @Override
    public ServiceVO save(Goods goods, Integer goodsId) {
        if (goodsId == null){// 添加操作
            goodsDao.insert(goods);
        } else { // 修改操作
            goodsDao.update(goods,goodsId);
        }

        return new ServiceVO(100,"请求成功",null);
    }

    @Override
    public ServiceVO delete(Integer goodsId) {
        Goods goods = goodsDao.findByGoodsId(goodsId);
        if (goods.getState() == 1) {
            return new ServiceVO<>(ErrorCode.STORED_ERROR_CODE, ErrorCode.STORED_ERROR_MESS);
        } else if (goods.getState() == 2) {
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        } else {
            goodsDao.delete(goodsId);
        }
        return new ServiceVO(100,"请求成功",null);
    }

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String,Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int pageNo = (page - 1) * rows;
        List<Goods> goodsList =  goodsDao.getNoInventoryQuantity(pageNo,rows,nameOrCode);
        map.put("total",goodsList.size());
        map.put("rows",goodsList);
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String,Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int pageNo = (page - 1) * rows;
        List<Goods> goodsList =  goodsDao.getHasInventoryQuantity(pageNo,rows,nameOrCode);
        map.put("total",goodsList.size());
        map.put("rows",goodsList);
        return map;
    }

    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        Goods goods = goodsDao.findByGoodsId(goodsId);
        double oldPurchasingPrice = goods.getPurchasingPrice();
        goods.setLastPurchasingPrice(oldPurchasingPrice);
        goods.setPurchasingPrice(purchasingPrice);
        goods.setInventoryQuantity(inventoryQuantity);
        goodsDao.saveStock(goods);
        return new ServiceVO(100,"请求成功",null);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Goods goods = goodsDao.findByGoodsId(goodsId);
        Integer state = goods.getState();
        if (state == 2){
            return new ServiceVO<>(ErrorCode.HAS_FORM_ERROR_CODE, ErrorCode.HAS_FORM_ERROR_MESS);
        }
        goods.setInventoryQuantity(0);
        goodsDao.updateGoods(goods);
        return new ServiceVO(100,"请求成功",null);
    }

    @Override
    public Map<String, Object> listAlarm() {
        Map<String,Object> map = new HashMap<>();
        List<Goods> goodsList =  goodsDao.findListAlarm();
        map.put("rows",goodsList);
        return map;
    }
}
