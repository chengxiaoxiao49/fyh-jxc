package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public ServiceVO save(DamageList damageList, String damageListGoodsStr) {
        // 获取用户id
        User user = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());
        Integer userId = user.getUserId();
        Gson gson = new Gson();
        List<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr,new TypeToken<List<DamageListGoods>>(){}.getType());
        damageList.setUserId(userId);
        // 保存报损订单信息
        damageListGoodsDao.saveDamageList(damageList);
        for (DamageListGoods damageListGoods : damageListGoodsList) {
            damageListGoods.setDamageListId(damageList.getDamageListId());
            damageListGoodsDao.saveDamageListGoods(damageListGoods);

            Goods goods = goodsDao.findByGoodsId(damageListGoods.getGoodsId());
            goods.setInventoryQuantity(goods.getInventoryQuantity()-damageListGoods.getGoodsNum());
            goods.setState(2);
            goodsDao.updateGoods(goods);
        }
        return new ServiceVO(100,"请求成功",null);
    }


    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        Map<String, Object> map = new HashMap<>();
        List<DamageList> damageListGoodsList = damageListGoodsDao.findByTimeList(sTime,eTime);
        User user = userDao.findUserByName((String) SecurityUtils.getSubject().getPrincipal());
        List<DamageList> damageListList = damageListGoodsList.stream().map(damageList -> {
            damageList.setTrueName(user.getTrueName());
            return damageList;
        }).collect(Collectors.toList());
        map.put("rows",damageListList);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String, Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.findByDamageListId(damageListId);
        map.put("rows",damageListGoodsList);
        return map;
    }


}
