package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

import java.util.ArrayList;
import java.util.Map;

/**
 * @description
 */

public interface GoodsTypeService {
    ArrayList<Object> loadGoodsType();

    ServiceVO save(String goodsTypeName, Integer pId);

    ServiceVO delete(Integer goodsTypeId);

}
