package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping("/list")
    public Map<String,Object> list(Integer page, Integer rows, String  customerName){
        return customerService.list(page,rows,customerName);
    }

    @PostMapping("/save")
    public ServiceVO save(Customer customer,@RequestParam(value = "customerId" ,required = false) Integer customerId){
        return customerService.save(customer,customerId);
    }

    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam("ids") String  ids){
        return customerService.delete(ids);
    }
}
