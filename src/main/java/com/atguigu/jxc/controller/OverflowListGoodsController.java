package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    private OverflowListGoodsService overflowListGoodsService;

    @PostMapping("/save")
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr){
        return overflowListGoodsService.save(overflowList,overflowListGoodsStr);
    }

    @PostMapping("/list")
    public Map<String,Object> list(String  sTime, String  eTime){
        return overflowListGoodsService.list(sTime,eTime);
    }

    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer overflowListId){
        return overflowListGoodsService.goodsList(overflowListId);
    }

}
