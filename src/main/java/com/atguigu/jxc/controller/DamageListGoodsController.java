package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListGoodsService damageListGoodsService;


    @PostMapping("/save")
    public ServiceVO save(DamageList damageList, String damageListGoodsStr){
        return damageListGoodsService.save(damageList,damageListGoodsStr);
    }

    @PostMapping("/list")
    public Map<String,Object> list(String  sTime, String  eTime){
        return damageListGoodsService.list(sTime,eTime);
    }

    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        return damageListGoodsService.goodsList(damageListId);
    }
}
