package com.atguigu.jxc.controller;


import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
@RestController
@RequestMapping("/goods")
@Slf4j
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @PostMapping("/listInventory")
    public Map<String,Object> listInventory(Integer page,
                                            Integer rows,
                                            String codeOrName,
                                            Integer goodsTypeId){
        Map<String,Object>  map = goodsService.listInventory(page,rows,codeOrName,goodsTypeId);
        return map;
    }

    @PostMapping("/list")
    private Map<String,Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId){
        log.error("GoodsController ------->  list 接口实现");
        return goodsService.list(page,rows,goodsName,goodsTypeId);
    }

    @PostMapping("/save")
    private ServiceVO save(Goods goods,@RequestParam(value = "goodsId",required = false) Integer goodsId){
        return goodsService.save(goods,goodsId);
    }

    @PostMapping("/delete")
    private ServiceVO delete(Integer goodsId){
        return goodsService.delete(goodsId);
    }

    @PostMapping("/getCode")
    private ServiceVO getCode(){
        return goodsService.getCode();
    }

    @PostMapping("/getNoInventoryQuantity")
    private Map<String,Object> getNoInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        return goodsService.getNoInventoryQuantity(page,rows,nameOrCode);
    }

    @PostMapping("/getHasInventoryQuantity")
    private Map<String,Object> getHasInventoryQuantity(Integer page,Integer rows,String nameOrCode){
        return goodsService.getHasInventoryQuantity(page,rows,nameOrCode);
    }

    @PostMapping("/saveStock")
    private ServiceVO saveStock(Integer goodsId,Integer inventoryQuantity,double purchasingPrice){
        return goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
    }

    @PostMapping("/deleteStock")
    private ServiceVO deleteStock(Integer goodsId) {
        return goodsService.deleteStock(goodsId);
    }

    @PostMapping("/listAlarm")
    private Map<String,Object> listAlarm() {
        return goodsService.listAlarm();
    }
}
