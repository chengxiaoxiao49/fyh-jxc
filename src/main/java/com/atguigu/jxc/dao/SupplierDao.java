package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface SupplierDao {


    List<Supplier> getSupplierPageList(@Param("pageNo") int pageNo, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    void insert(@Param("supplier") Supplier supplier);

    void update(@Param("supplier") Supplier supplier, @Param("supplierId") Integer supplierId);

    Supplier getSupplierById(Integer supplierId);

    void delete(@Param("ids") String ids);
}
