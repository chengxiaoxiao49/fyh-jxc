package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DamageListGoodsDao {


    void saveDamageListGoods(@Param("damageListGoods") DamageListGoods damageListGoods);


    void saveDamageList( DamageList damageList);

    List<DamageList> findByTimeList(@Param("sTime") String sTime,@Param("eTime") String eTime);

    List<DamageListGoods> findByDamageListId(@Param("damageListId") Integer damageListId);
}
