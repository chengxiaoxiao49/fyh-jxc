package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {

    List<Customer> findCustomerDaoList(@Param("pageNo") int pageNo,@Param("rows") Integer rows,@Param("customerName") String customerName);

    void insert(@Param("customer") Customer customer);

    void update(Customer customer, Integer customerId);

    void delete(String ids);
}
