package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OverflowListGoodsDao {


    void saveOverflowList(OverflowList overflowList);

    void saveOverflowListGoods(OverflowListGoods overflowListGoods);

    List<OverflowList> findByTimeList(@Param("sTime") String sTime,@Param("eTime") String eTime);

    List<OverflowListGoods> findByOverflowListId(Integer overflowListId);
}
