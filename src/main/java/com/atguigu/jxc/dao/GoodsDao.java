package com.atguigu.jxc.dao;


import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.PurchaseList;
import com.atguigu.jxc.entity.SaleListGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GoodsDao {


    List<Goods> findNameAndTypeId(@Param("pageNo") int pageNo, @Param("rows") Integer rows,@Param("codeOrName")  String codeOrName,@Param("goodsTypeId")  Integer goodsTypeId);

    int findTotal(@Param("codeOrName") String codeOrName,@Param("goodsTypeId") Integer goodsTypeId);

    Integer getSaleTotal(Integer goodsId);

    void insert(@Param("goods") Goods goods);

    void delete(@Param("goodsId") Integer goodsId);

    void update(@Param("goods") Goods goods,@Param("goodsId") Integer goodsId);

    String getMaxCode();

    List<Goods> getNoInventoryQuantity(@Param("pageNo") int pageNo, @Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("pageNo") int pageNo, @Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    void saveStock(@Param("goods") Goods goods);

    Goods findByGoodsId(@Param("goodsId") Integer goodsId);

    void updateGoods(@Param("goods") Goods goods);

    List<Goods> findListAlarm();
}
